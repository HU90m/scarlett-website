import preprocess from 'svelte-preprocess';
import static_site from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),
	kit: {
		adapter: static_site(),

		prerender: {
			default: true,
		}
	}
};

export default config;
