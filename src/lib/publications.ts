export type Publication = {
  title: string;
  thumbnail: string;
  document_link: string;
  web_link: string | null;
  topic: string;
};

export const first_author: Publication[] = [
  {
    title: "Lean pathways in orthopaedics: multiple wins for sustainability",
    thumbnail: "/thumbnail/lean-pathways-in-orthopaedics.png",
    web_link: null,
    document_link: "/document/lean-pathways-in-orthopaedics.pdf",
    topic: "Sustainability",
  },
  {
    title:
      "The need to improve opioid prescribing and data collection in patients undergoing orthopaedic surgery",
    thumbnail:
      "/thumbnail/need-to-improve-opioid-prescribing-and-data-collection-in-patients.png",
    web_link: null,
    document_link:
      "/document/need-to-improve-opioid-prescribing-and-data-collection-in-patients.pdf",
    topic: "Perioperative care",
  },
  {
    title:
      "Developing the first pan-UK multi-professional guideline for perioperative management of anaemia",
    thumbnail:
      "/thumbnail/developing-guidelines-perioperative-anaemia-poster.png",
    web_link: null,
    document_link:
      "/document/developing-guidelines-perioperative-anaemia-poster.pdf",
    topic: "Perioperative care",
  },
  {
    title:
      "Preoperative assessment and optimisation: the key to good outcomes after the pandemic",
    thumbnail:
      "/thumbnail/Preoperative-assessment-and-optimisation-the-key-to-good-outcomes-after-the-pandemic.png",
    web_link:
      "https://www.magonlinelibrary.com/doi/full/10.12968/hmed.2021.0318",
    document_link:
      "/document/Preoperative-assessment-and-optimisation-the-key-to-good-outcomes-after-the-pandemic.pdf",
    topic: "Perioperative care",
  },
  {
    title: "Focus on physical activity can help avoid unnecessary social care",
    thumbnail:
      "/thumbnail/Focus-on-physical-activity-avoid-unnecessary-social-care.png",
    web_link: "https://www.bmj.com/content/359/bmj.j4609",
    document_link:
      "/document/Focus-on-physical-activity-avoid-unnecessary-social-care.pdf",
    topic: "Exercise",
  },
  {
    title:
      "Exercise the miracle cure and the role of the doctor in promoting it.",
    thumbnail: "/thumbnail/Exercise-miracle-cure-exercise-role-of-doctors.png",
    web_link:
      "https://www.aomrc.org.uk/reports-guidance/exercise-the-miracle-cure-0215/",
    document_link:
      "/document/Exercise-miracle-cure-exercise-role-of-doctors.pdf",
    topic: "Exercise",
  },
  {
    title:
      "Exercise: the miracle cure for surgeons to fix the NHS and social care ",
    thumbnail: "/thumbnail/Exercise-miracle-cure-for-surgeons.png",
    web_link: "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/rcsbull.2020.28",
    document_link: "/document/Exercise-miracle-cure-for-surgeons.pdf",
    topic: "Exercise",
  },
  {
    title:
      "Developing a ‘Doctors’ Assistant’ role to ease pressure on doctors and improve patient flow in acute NHS hospitals",
    thumbnail: "/thumbnail/Developing-a-doctor-assistant-role.png",
    web_link: "https://bmjleader.bmj.com/content/5/1/62",
    document_link: "/document/Developing-a-doctor-assistant-role.pdf",
    topic: "Doctors' Assistants",
  },
  {
    title:
      "Award-winning new doctors' assistants freeing time in acute NHS hospitals",
    thumbnail:
      "/thumbnail/award-winning-new-doctor-s-assistants-freeing-time-in-acute.png",
    web_link:
      "https://healthmanagement.org/c/healthmanagement/issuearticle/award-winning-new-doctors-assistants-freeing-time-in-acute-nhs-hospitals",
    document_link:
      "/document/award-winning-new-doctor-s-assistants-freeing-time-in-acute.pdf",
    topic: "Doctors' Assistants",
  },
  {
    title: "Clarity at Twilight",
    thumbnail: "/thumbnail/Clarity-at-twilight.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/full/10.1308/rcsbull.2019.124",
    document_link: "/document/Clarity-at-twilight.pdf",
    topic: "Myeloma",
  },
  {
    title: "Surgical Training: Still Highly Competitive But Still Very Male",
    thumbnail:
      "/thumbnail/surgical-training-still-highly-competitive-but-still-very-male.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363512X13189526438675",
    document_link:
      "/document/surgical-training-still-highly-competitive-but-still-very-male.pdf",
    topic: "Surgical career",
  },
  {
    title:
      "Competition ratios for different specialties and the effect of gender and immigration status",
    thumbnail: "/thumbnail/competition-ratios-for-different-specialties.png",
    web_link: "https://journals.sagepub.com/doi/full/10.1258/jrsm.2008.070284",
    document_link: "/document/competition-ratios-for-different-specialties.pdf",
    topic: "Surgical career",
  },
  {
    title: "WiST and WinS across a surgical career",
    thumbnail: "/thumbnail/WiST-and-WinS-across-a-surgical-career.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/full/10.1308/rcsbull.2021.107",
    document_link: "/document/WiST-and-WinS-across-a-surgical-career.pdf",
    topic: "Surgical career",
  },
  {
    title: "The pregnant surgeon letter",
    thumbnail: "/thumbnail/The-pregnant-surgeon-letter.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363506X118618",
    document_link: "/document/The-pregnant-surgeon-letter.pdf",
    topic: "Surgical career",
  },
  {
    title: "Win–win for sustainability and health",
    thumbnail: "/thumbnail/win-win-for-sustainability-and-health.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/rcsbull.2020.146",
    document_link: "/document/win-win-for-sustainability-and-health.pdf",
    topic: "Sustainability",
  },
  {
    title:
      "Harnessing perioperative care, prevention and cycling to improve sustainability in surgery",
    thumbnail:
      "/thumbnail/Harnessing-perioperative-care-prevention-and-cycling-to-improve-sustainability-in-surgery.png",
    web_link:
      "https://www.asgbi.org.uk/userfiles/file/journals/winter-2020-jasgbi-surgical-life.pdf",
    document_link:
      "/document/Harnessing-perioperative-care-prevention-and-cycling-to-improve-sustainability-in-surgery.pdf",
    topic: "Sustainability",
  },
  {
    title: "Global population explosion- economic and health meltdown",
    thumbnail:
      "/thumbnail/Global-population-explosion-economic-and-health-meltdown.png",
    web_link: "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3569013/",
    document_link:
      "/document/Global-population-explosion-economic-and-health-meltdown.pdf",
    topic: "Global",
  },
  {
    title:
      "Comparing booking systems for elective orthopaedic surgery: partial booking reduced cancellation from 56% to 8%",
    thumbnail:
      "/thumbnail/Comparing-booking-systems-for-elective-orthopaedic-surgery-partial-booking-reduced-cancellation-from-56-to-8.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363507X204747",
    document_link:
      "/document/Comparing-booking-systems-for-elective-orthopaedic-surgery-partial-booking-reduced-cancellation-from-56-to-8.pdf",
    topic: "Processes",
  },
];

export const co_author: Publication[] = [
  {
    title: "Are single use items the biggest scam of the century?",
    thumbnail:
      "/thumbnail/are-single-use-items-the-biggest-scam-of-the-century.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/full/10.1308/rcsbull.2021.89",
    document_link:
      "/document/are-single-use-items-the-biggest-scam-of-the-century.pdf",
    topic: "Sustainability",
  },
  {
    title:
      "What are the differences between medical schools that graduate more aspiring surgeons than others?",
    thumbnail:
      "/thumbnail/what-are-the-differences-between-medical-schools-that-graduate-more-aspiring-surgeons-than-others.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/full/10.1308/rcsbull.2020.e009",
    document_link:
      "/document/what-are-the-differences-between-medical-schools-that-graduate-more-aspiring-surgeons-than-others.pdf",
    topic: "Surgical career",
  },
  {
    title:
      "Does choice of medical school affect a student’s likelihood of becoming a surgeon?",
    thumbnail:
      "/thumbnail/does-choice-of-medical-school-affect-a-student’s-likelihood-of-becoming-a-surgeon.png",
    web_link: "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/rcsbull.2018.90",
    document_link:
      "/document/does-choice-of-medical-school-affect-a-student’s-likelihood-of-becoming-a-surgeon.pdf",
    topic: "Surgical career",
  },
  {
    title: "Sampling specialties - taster days for UK Foundation doctors",
    thumbnail: "/thumbnail/sampling-specialities-taster-week-article.png",
    web_link: null,
    document_link: "/document/sampling-specialities-taster-week-article.pdf",
    topic: "Surgical career",
  },
  {
    title: "Stress and the surgeon",
    thumbnail: "/thumbnail/stress-and-the-surgeon.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/rcsbull.2020.216",
    document_link: "/document/stress-and-the-surgeon.pdf",
    topic: "Surgical career",
  },
  {
    title: "Instinct, intuition and surgical decision-making",
    thumbnail: "/thumbnail/instinct-intuition-and-surgical-decision-making.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/full/10.1308/rcsbull.2015.345",
    document_link:
      "/document/instinct-intuition-and-surgical-decision-making.pdf",
    topic: "Surgical career",
  },
  {
    title: "Developing the Best Surgeons",
    thumbnail: "/thumbnail/developing-the-best-surgeons.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363513X13690603818066",
    document_link: "/document/developing-the-best-surgeons.pdf",
    topic: "Surgical career",
  },
  {
    title: "who’s really in charge in the operating theatre?",
    thumbnail: "/thumbnail/whos-really-in-charge-in-the-operating-theatre.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363512X13448516927468",
    document_link:
      "/document/whos-really-in-charge-in-the-operating-theatre.pdf",
    topic: "Surgical career",
  },
  {
    title: "Medical student liaison",
    thumbnail: "/thumbnail/medical-student-liaison.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/rcsbull.2015.448",
    document_link: "/document/medical-student-liaison.pdf",
    topic: "Surgical career",
  },
  {
    title: "Revalidation",
    thumbnail: "/thumbnail/revalidation.png",
    web_link:
      "https://publishing.rcseng.ac.uk/doi/pdf/10.1308/147363513X13690603818589",
    document_link: "/document/revalidation.pdf",
    topic: "Surgical career",
  },
  {
    title: "Changing the norm towards gender equity in surgery",
    thumbnail:
      "/thumbnail/changing-the-norm-towards-gender-equity-in-surgery.png",
    web_link: "https://journals.sagepub.com/eprint/QAKHCTDTHJT9X2DSIQ3K/full",
    document_link:
      "/document/changing-the-norm-towards-gender-equity-in-surgery.pdf",
    topic: "Surgical career",
  },
  {
    title:
      "Are there gender differences in general surgical operative experience?",
    thumbnail:
      "/thumbnail/are-there-gender-differences-in-general-surgical-operative-experience.png",
    web_link:
      "https://www.sciencedirect.com/science/article/pii/S1743919111005164",
    document_link:
      "/document/are-there-gender-differences-in-general-surgical-operative-experience.pdf",
    topic: "Surgical career",
  },
];
